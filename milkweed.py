import turtle


w = turtle.Screen()
w.clear()
w.bgcolor("#FFFFFF")
t = turtle.Turtle()
t.width(5)
t.speed (2)
for n in range(0,180,15):
	t.pencolor("#FF3300")
	t.goto(0,0)
	t.forward(100)
	t.seth(n)

w.exitonclick()
